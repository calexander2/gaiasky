# Gaia Sky VR

*This file is only concerned with Gaia Sky VR. If you are looking for the regular desktop Gaia Sky, [check this out](README.md).*

The most up-to-date information on Gaia Sky VR is available in the official documentation:

- [Gaia Sky VR documentation](http://gaia.ari.uni-heidelberg.de/gaiasky/docs/html/latest/Gaia-sky-vr.html)

##  More info

The project's main README file is [here](README.md).
