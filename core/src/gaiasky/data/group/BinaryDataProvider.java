/*
 * This file is part of Gaia Sky, which is released under the Mozilla Public License 2.0.
 * See the file LICENSE.md in the project root for full license details.
 */

package gaiasky.data.group;

import com.badlogic.gdx.utils.Array;
import gaiasky.scenegraph.ParticleGroup.ParticleBean;
import gaiasky.scenegraph.StarGroup.StarBean;
import gaiasky.util.Constants;
import gaiasky.util.GlobalConf;
import gaiasky.util.I18n;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Reads arrays of star beans from binary files, usually to go in an octree.
 *
 * @author tsagrista
 */
public class BinaryDataProvider extends AbstractStarGroupDataProvider {

    @Override
    public Array<ParticleBean> loadData(String file) {
        return loadData(file, 1d);
    }

    @Override
    public Array<ParticleBean> loadData(String file, double factor) {
        logger.info(I18n.bundle.format("notif.datafile", file));
        loadDataMapped(file, factor);
        logger.info(I18n.bundle.format("notif.nodeloader", list.size, file));

        return list;
    }

    @Override
    public Array<ParticleBean> loadData(InputStream is, double factor) {
        list = readData(is);
        return list;
    }

    public void writeData(Array<? extends ParticleBean> data, OutputStream out) {
        // Wrap the FileOutputStream with a DataOutputStream
        DataOutputStream data_out = new DataOutputStream(out);
        try {
            // Size of stars
            data_out.writeInt(data.size);
            for (ParticleBean sb : data) {
                writeStarBean((StarBean) sb, data_out);
            }

        } catch (Exception e) {
            logger.error(e);
        } finally {
            try {
                data_out.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }

    }

    protected void writeStarBean(StarBean sb, DataOutputStream out) throws IOException {
        // Double
        for (int i = 0; i < StarBean.I_APPMAG; i++) {
            out.writeDouble(sb.data[i]);
        }
        // Float
        for (int i = StarBean.I_APPMAG; i < StarBean.I_HIP; i++) {
            out.writeFloat((float) sb.data[i]);
        }
        // Int
        out.writeInt((int) sb.data[StarBean.I_HIP]);

        // 3 integers, keep compatibility
        out.writeInt(-1);
        out.writeInt(-1);
        out.writeInt(-1);

        out.writeLong(sb.id);

        String namesConcat = sb.namesConcat();
        out.writeInt(namesConcat.length());
        out.writeChars(namesConcat);
    }

    public Array<ParticleBean> readData(InputStream in) {
        Array<ParticleBean> data = null;
        DataInputStream data_in = new DataInputStream(in);

        try {
            // Read size of stars
            int size = data_in.readInt();
            data = new Array<>(size);
            for (int i = 0; i < size; i++) {
                data.add(readStarBean(data_in));
            }

        } catch (IOException e) {
            logger.error(e);
        } finally {
            try {
                data_in.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }

        return data;
    }

    protected StarBean readStarBean(DataInputStream in) throws IOException {
        double[] data = new double[StarBean.SIZE];
        // Double
        for (int i = 0; i < StarBean.I_APPMAG; i++) {
            data[i] = in.readDouble();
            if (i < 6)
                data[i] *= Constants.DISTANCE_SCALE_FACTOR;
        }
        // Float
        for (int i = StarBean.I_APPMAG; i < StarBean.I_HIP; i++) {
            data[i] = in.readFloat();
            if (i == StarBean.I_SIZE)
                data[i] *= Constants.DISTANCE_SCALE_FACTOR;
        }
        // Int
        data[StarBean.I_HIP] = in.readInt();

        // Skip unused tycho numbers, 3 Integers
        in.readInt();
        in.readInt();
        in.readInt();

        Long id = in.readLong();
        int nameLength = in.readInt();
        StringBuilder namesConcat = new StringBuilder();
        for (int i = 0; i < nameLength; i++)
            namesConcat.append(in.readChar());
        String[] names = namesConcat.toString().split(Constants.nameSeparator);
        return new StarBean(data, id, names);
    }

    public Array<ParticleBean> loadDataMapped(String file, double factor) {
        try {
            FileChannel fc = new RandomAccessFile(GlobalConf.data.dataFile(file), "r").getChannel();

            MappedByteBuffer mem = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            // Read size of stars
            int size = mem.getInt();
            list = new Array<>(size);
            for (int i = 0; i < size; i++) {
                list.add(readStarBean(mem));
            }

            fc.close();

            return list;

        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public StarBean readStarBean(MappedByteBuffer mem) {
        double[] data = new double[StarBean.SIZE];
        // Double
        for (int i = 0; i < StarBean.I_APPMAG; i++) {
            data[i] = mem.getDouble();
            if (i < 6)
                data[i] *= Constants.DISTANCE_SCALE_FACTOR;
        }
        // Float
        for (int i = StarBean.I_APPMAG; i < StarBean.I_HIP; i++) {
            data[i] = mem.getFloat();
            if (i == StarBean.I_SIZE)
                data[i] *= Constants.DISTANCE_SCALE_FACTOR;
        }
        // Int
        data[StarBean.I_HIP] = mem.getInt();

        // Skip unused tycho numbers, 3 Integers
        mem.getInt();
        mem.getInt();
        mem.getInt();

        Long id = mem.getLong();
        int nameLength = mem.getInt();
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < nameLength; i++)
            name.append(mem.getChar());

        return new StarBean(data, id, name.toString());
    }

}
